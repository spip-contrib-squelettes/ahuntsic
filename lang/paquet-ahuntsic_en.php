<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-ahuntsic
// Langue: en
// Date: 18-07-2023 16:27:11
// Items: 1

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ahuntsic_description' => 'Activate this plugin to enable {{Ahuntsic}} as the default template of your site.',
);
?>